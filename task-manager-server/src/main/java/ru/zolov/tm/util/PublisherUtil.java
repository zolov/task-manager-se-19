package ru.zolov.tm.util;

import java.util.Set;
import javax.xml.ws.Endpoint;
import ru.zolov.tm.endpoint.AbstractEndpoint;

public class PublisherUtil {

  private PublisherUtil() {}

  public static void publish(Set<AbstractEndpoint> endpoints) {
    for (AbstractEndpoint endpoint : endpoints) {
      publish(endpoint);
    }
  }

  public static void publish(AbstractEndpoint endpoint) {
    String endpointName = endpoint.getClass().getSimpleName();
    String link = endpoint.getUrl() + endpointName + endpoint.getPostfix();
    System.out.println("[" + endpointName + "]" + " published at " + link);
    Endpoint.publish(link, endpoint);
  }
}
