package ru.zolov.tm.endpoint;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.api.ISessionService;
import ru.zolov.tm.api.ITaskService;
import ru.zolov.tm.api.IUserService;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.dto.TaskDto;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;


@NoArgsConstructor
@WebService
@Component
public class TaskEndpoint extends AbstractEndpoint {
  @Autowired
  private ITaskService taskService;
  @Autowired
  private ISessionService sessionService;
  @Autowired
  private IProjectService projectService;
  @Autowired
  private IUserService userService;


  @SneakyThrows @NotNull @WebMethod public TaskDto addNewTask(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "projectId") final String projectId,
      @NotNull @WebParam(name = "name") final String name,
      @NotNull @WebParam(name = "description") final String description,
      @NotNull @WebParam(name = "start") final String start,
      @NotNull @WebParam(name = "finish") final String finish
  )  {
    if (sessionDto.getUserId() == null) throw new AccessForbiddenException();
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    @NotNull final Project project = projectService.findByUserAndId(user.getId(), projectId);
    Task task = taskService.create(user, project, name, description, start, finish);
    sessionService.validate(session);

    return taskService.transformTaskToDto(task);
  }

  @SneakyThrows @NotNull @WebMethod public List<TaskDto> findAllTasksByUserId(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto
  ) {
    if (sessionDto.getUserId() == null) throw new AccessForbiddenException();
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    @NotNull final List<Task> taskList = taskService.findAllByUserId(sessionDto.getUserId());
    return taskService.transformListOfTask(taskList);
  }

  @SneakyThrows @NotNull @WebMethod public List<TaskDto> findTasksByProjectId(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "projectId") final String projectId
  ) {
    if (sessionDto.getUserId() == null) throw new AccessForbiddenException();
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    @NotNull final Project project = projectService.findByUserAndId(user.getId(), projectId);
    sessionService.validate(session);
    @NotNull final List<Task> taskList = taskService.findTaskByProjectId(sessionDto.getUserId(), projectId);
    return taskService.transformListOfTask(taskList);
  }

  @SneakyThrows @Nullable @WebMethod public TaskDto findOneTaskById(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "projectId") final String projectId,
      @NotNull @WebParam(name = "taskId") final String taskId
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    @NotNull final Task task = taskService.findTaskById(sessionDto.getUserId(), taskId);
    return taskService.transformTaskToDto(task);
  }

  @SneakyThrows @WebMethod public void removeOneTaskById(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "taskId") final String taskId
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    taskService.removeTaskById(sessionDto.getUserId(), taskId);
  }

  @SneakyThrows @WebMethod public void updateTask(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "taskId") final String taskId,
      @NotNull @WebParam(name = "taskName") final String taskName,
      @NotNull @WebParam(name = "taskDescription") final String taskDescription,
      @NotNull @WebParam(name = "dateOfstart") final String dateOfstart,
      @NotNull @WebParam(name = "dateOffinish") final String dateOffinish
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    taskService.update(user, taskId, taskName, taskDescription, dateOfstart, dateOffinish);
  }

  @SneakyThrows @NotNull @WebMethod public List<TaskDto> findTaskByPartOfTheName(
      @NotNull @WebParam(name = "session") final SessionDto sessionDto,
      @NotNull @WebParam(name = "partOfTheName") final String partOfTheName
  ) {
    @NotNull final User user = userService.findOneById(sessionDto.getUserId());
    @NotNull final Session session = sessionService.transformDtoToSession(sessionDto, user);
    sessionService.validate(session);
    @NotNull final List<Task> taskList = taskService.findTask(sessionDto.getUserId(), partOfTheName);

    return taskService.transformListOfTask(taskList);
  }

}
