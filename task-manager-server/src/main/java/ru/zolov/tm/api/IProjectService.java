package ru.zolov.tm.api;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.dto.ProjectDto;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IProjectService {

  @NotNull Project create(
      @Nullable User user,
      @Nullable String name,
      @Nullable String description,
      @Nullable String start,
      @Nullable String finish
  ) throws EmptyStringException, ParseException, SQLException;

  @NotNull Project findByUserAndId(
      @Nullable String userId,
      @Nullable String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull List<Project> findAllByUserId(@Nullable String userId) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull List<Project> findAll(Session session) throws EmptyStringException, EmptyRepositoryException, AccessForbiddenException, SQLException;

  void update(
      @Nullable User user,
      @Nullable String id,
      @Nullable String name,
      @Nullable String description,
      @Nullable String start,
      @Nullable String finish
  ) throws ParseException, SQLException, EmptyStringException, EmptyRepositoryException;

  void removeById(
      @Nullable String userId,
      @Nullable String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException;

  @NotNull List<Project> findProject(
      @Nullable String userId,
      @Nullable String partOfTheName
  ) throws EmptyRepositoryException, EmptyStringException, SQLException;

  void load(@NotNull Session session, @Nullable List<Project> list) throws EmptyRepositoryException, SQLException, EmptyStringException, AccessForbiddenException;

  @NotNull ProjectDto transformProjectToDto(@NotNull Project project);

  @NotNull Project transformDtoToProject(@NotNull ProjectDto projectDto, @NotNull User user);

  List<ProjectDto> transformListProject(@NotNull List<Project> projectList);
}
