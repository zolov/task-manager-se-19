package ru.zolov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.zolov.tm.entity.Task;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {

  @Query("SELECT t FROM Task t WHERE t.project.id = :projectId AND t.user.id = :userId")
  List<Task> findAllByProjectIdUserId(@NotNull @Param("userId") String userId, @NotNull  @Param("projectId") String projectId);

  @Query("SELECT t FROM Task t WHERE t.user.id = :userId")
  List<Task> findAllByUserId(@NotNull @Param("userId") String userId);

  @Query("SELECT t FROM Task t WHERE t.user.id = :userId  AND t.id = :id")
  Optional<Task> findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

  @Query("SELECT t FROM Task t WHERE t.project.id = :projectId")
  List <Task> findAllTasksByProjectId(@NotNull @Param("projectId") String projectId);

  @Transactional
  @Modifying
  @Query("DELETE FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId")
  void deleteAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

  @Query("SELECT t FROM Task t WHERE t.user.id = :userId AND (t.name LIKE CONCAT ('%',:string,'%') OR t.description LIKE CONCAT ('%',:string,'%'))")
  List<Task> findTaskByPathOfTheName(@NotNull @Param("userId") String userId, @Param("string")@NotNull String string);

  @Transactional
  @Modifying
  @Query("DELETE FROM Task t WHERE t.user.id= :userId")
  void removeAllByUserId(@NotNull @Param("userId") String userId);
}
