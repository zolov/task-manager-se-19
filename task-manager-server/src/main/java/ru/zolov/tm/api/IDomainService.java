package ru.zolov.tm.api;

import java.sql.SQLException;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.dto.DomainDto;
import ru.zolov.tm.dto.SessionDto;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IDomainService {

  void load(@NotNull SessionDto sessionDto, @NotNull DomainDto domainDto) throws EmptyStringException, EmptyRepositoryException, SQLException;

  void save(@NotNull SessionDto sessionDto, @NotNull DomainDto domainDto) throws EmptyStringException, EmptyRepositoryException, SQLException;
}
