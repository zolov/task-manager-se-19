package ru.zolov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.zolov.tm.configuration.PersistenceJPAConfig;
import ru.zolov.tm.loader.Bootstrap;

public class Application {

  public static void main(String[] args) throws Exception {
    @NotNull ApplicationContext context = new AnnotationConfigApplicationContext(PersistenceJPAConfig.class);
    @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
    bootstrap.init();
  }
}
