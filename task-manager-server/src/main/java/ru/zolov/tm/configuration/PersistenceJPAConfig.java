package ru.zolov.tm.configuration;

import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan("ru.zolov.tm")
@EnableJpaRepositories(basePackages = "ru.zolov.tm.api")
@EnableTransactionManagement(proxyTargetClass = true)
public class PersistenceJPAConfig {

  @Bean public DataSource dataSource() {
    final DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("com.mysql.jdbc.Driver");
    dataSource.setUrl("jdbc:mysql://localhost:3306/tmdb_zolov?serverTimezone=UTC");
    dataSource.setUsername("root");
    dataSource.setPassword("root");
    return dataSource;
  }

  @Bean public LocalContainerEntityManagerFactoryBean entityManagerFactory(final DataSource dataSource) {
    LocalContainerEntityManagerFactoryBean factoryBean;
    factoryBean = new LocalContainerEntityManagerFactoryBean();
    factoryBean.setDataSource(dataSource);
    factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
    factoryBean.setPackagesToScan("ru.zolov.tm");
    Properties properties = new Properties();
    properties.put("hibernate.show.sql", "true");
    properties.put("hibernate.hdm2ddl.auto", "update");
    properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
    factoryBean.setJpaProperties(properties);
    return factoryBean;
  }

  @Bean public PlatformTransactionManager transactionManager(final LocalContainerEntityManagerFactoryBean emf) {
    final JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(emf.getObject());
    return transactionManager;
  }
}
