package ru.zolov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.entity.User;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
@XmlRootElement
public class DomainDto implements Serializable {

  @NotNull private List<Project> project = new ArrayList<>();
  @NotNull private List<Task> task = new ArrayList<>();
  @NotNull private List<User> user = new ArrayList<>();
}
