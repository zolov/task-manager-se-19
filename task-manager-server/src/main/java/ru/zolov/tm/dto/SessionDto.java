package ru.zolov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.enumerated.RoleType;

@Getter
@Setter
@NoArgsConstructor
public class SessionDto extends AbstractEntityDto implements Cloneable {

  @Nullable private String userId = "";
  @Nullable private String signature = "";
  @Nullable private Long timestamp = null;
  @Nullable private RoleType role = RoleType.USER;

  @Override public SessionDto clone() throws CloneNotSupportedException {
    return (SessionDto)super.clone();
  }
}
