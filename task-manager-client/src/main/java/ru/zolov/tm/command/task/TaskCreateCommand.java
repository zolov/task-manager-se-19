package ru.zolov.tm.command.task;

import java.text.ParseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.ParseException_Exception;
import ru.zolov.tm.api.Project;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.SQLException_Exception;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractCommand {

  private final String name = "task-create";
  private final String description = "Create new task";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws ParseException, EmptyStringException_Exception, CloneNotSupportedException_Exception, AccessForbiddenException_Exception, EmptyRepositoryException_Exception, ParseException_Exception, SQLException_Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    if (session == null) return;
    for (@NotNull final Project project : bootstrap.getProjectEndpoint().findAllProject(session)) {
      System.out.println(project);
    }
    System.out.print("Enter project id: ");
    final String projectId = TerminalUtil.nextLine();
    System.out.print("Enter name: ");
    final String name = TerminalUtil.nextLine();
    System.out.print("Enter description: ");
    final String description = TerminalUtil.nextLine();
    System.out.println("Enter date of project start in format DD.MM.YYYY: ");
    final String start = TerminalUtil.nextLine();
    System.out.println("Enter date of project finish in format DD.MM.YYYY: ");
    final String finish = TerminalUtil.nextLine();
    bootstrap.getTaskEndpoint().addNewTask(session, projectId, name, description, start, finish);
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
