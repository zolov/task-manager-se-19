package ru.zolov.tm.command.project;

import java.text.ParseException;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.ParseException_Exception;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.SQLException_Exception;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractCommand {

  private final String name = "project-create";
  private final String description = "Create new project";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws ParseException, EmptyStringException_Exception, AccessForbiddenException_Exception, CloneNotSupportedException_Exception, ParseException_Exception, EmptyRepositoryException_Exception, SQLException_Exception {
    @Nullable final Session session = bootstrap.getCurrentSession();
    if (session == null) return;

    System.out.print("Enter project name:");
    final String projectName = TerminalUtil.nextLine();
    System.out.print("Enter description: ");
    final String projectDescription = TerminalUtil.nextLine();
    System.out.println("Enter date of project start in format dd.MM.yyyy: ");
    final String start = TerminalUtil.nextLine();
    System.out.println("Enter date of project finish in format dd.MM.yyyy: ");
    final String finish = TerminalUtil.nextLine();
    bootstrap.getProjectEndpoint().createNewProject(session, projectName, projectDescription, start, finish);
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
