package ru.zolov.tm.command.user;

import ru.zolov.tm.api.AccessForbiddenException_Exception;
import ru.zolov.tm.api.CloneNotSupportedException_Exception;
import ru.zolov.tm.api.EmptyRepositoryException_Exception;
import ru.zolov.tm.api.EmptyStringException_Exception;
import ru.zolov.tm.api.RoleType;
import ru.zolov.tm.api.SQLException_Exception;
import ru.zolov.tm.api.Session;
import ru.zolov.tm.command.AbstractCommand;

public class UserLogOutCommand extends AbstractCommand {

  private final String name = "user-logout";
  private final String description = "User logout";

  @Override public String getName() {
    return name;
  }

  @Override public String getDescription() {
    return description;
  }

  @Override public boolean secure() {
    return false;
  }

  @Override public void execute() throws EmptyStringException_Exception, SQLException_Exception, CloneNotSupportedException_Exception, AccessForbiddenException_Exception, EmptyRepositoryException_Exception {
    final Session session = bootstrap.getCurrentSession();
    System.out.println("Session: " + session.getUserId() + " closed!");
    bootstrap.getSessionEndpoint().closeSesson(bootstrap.getCurrentSession());
  }

  @Override public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
